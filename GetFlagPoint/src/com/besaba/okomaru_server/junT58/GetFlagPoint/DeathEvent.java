package com.besaba.okomaru_server.junT58.GetFlagPoint;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

public class DeathEvent implements Listener{

	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		List<ItemStack> stacks = new ArrayList<ItemStack>();
		for(ItemStack stack : e.getDrops()){
			if(!stack.getType().equals(Material.SKULL_ITEM)){
				stacks.add(stack);
			}
		}
		for(ItemStack stack : stacks){
			e.getDrops().remove(stack);
		}
	}

}
