package com.besaba.okomaru_server.junT58.GetFlagPoint;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class GfpCommand implements CommandExecutor {

	private void giveitem(Player player, CommandSender sender){
		ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);
		ItemMeta smeta = sword.getItemMeta();
		smeta.addEnchant(Enchantment.DURABILITY, 3, true);
		sword.setItemMeta(smeta);
		ItemStack bow = new ItemStack(Material.BOW, 1);
		ItemMeta bmeta = bow.getItemMeta();
		bmeta.addEnchant(Enchantment.DURABILITY, 3, true);
		bmeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		bow.setItemMeta(bmeta);
		ItemStack arrow = new ItemStack(Material.ARROW, 1);
		ItemStack gapple = new ItemStack(Material.GOLDEN_APPLE, 3);
		ItemStack beef = new ItemStack(Material.COOKED_BEEF, 64);
		ItemStack hel = player.getInventory().getHelmet();
		player.getInventory().clear();
		if(hel != null){
			player.getInventory().setHelmet(hel);
		}
		player.getInventory().setItem(0, sword);
		player.getInventory().setItem(1, bow);
		player.getInventory().setItem(2, arrow);
		player.getInventory().setItem(3, gapple);
		player.getInventory().setItem(4, beef);
		sender.sendMessage(Main.Prefix + ChatColor.GOLD + player.getName() + "にアイテムを配布しました");
	}

	private List<String> usage(String cmd){
		List<String> list = new ArrayList<String>();
		list.add(Main.Prefix + ChatColor.RED + "引数が足りません！");
		list.add(Main.Prefix + ChatColor.GOLD + "====================");
		list.add(Main.Prefix + ChatColor.GOLD + "/" + cmd + " help");
		list.add(Main.Prefix + ChatColor.GOLD + "/" + cmd + " giveitems <player/all>");
		list.add(Main.Prefix + ChatColor.GOLD + "/" + cmd + " givearmor <player/all>");
		list.add(Main.Prefix + ChatColor.GOLD + "====================");
		return list;
	}

	private List<String> help(String cmd){
		List<String> list = new ArrayList<String>();
		list.add(Main.Prefix + ChatColor.GOLD + "====================");
		list.add(Main.Prefix + ChatColor.GOLD + "/" + cmd + " help");
		list.add(Main.Prefix + ChatColor.GOLD + "/" + cmd + " giveitems <player/all>");
		list.add(Main.Prefix + ChatColor.GOLD + "/" + cmd + " givearmor <player/all>");
		list.add(Main.Prefix + ChatColor.GOLD + "====================");
		return list;
	}

	private Color color(int r, int g, int b){
		Color color = Color.fromRGB(r, g, b);
		return color;
	}

	private ItemStack bluehelmet(){
		ItemStack helmet = new ItemStack(Material.LEATHER_HELMET, 1);
		Color c = color(0, 0, 255);
		LeatherArmorMeta lam = (LeatherArmorMeta) helmet.getItemMeta();
		lam.setColor(c);
		helmet.setItemMeta(lam);
		return helmet;
	}

	private ItemStack redhelmet(){
		ItemStack helmet = new ItemStack(Material.LEATHER_HELMET, 1);
		Color c = color(255, 0, 0);
		LeatherArmorMeta lam = (LeatherArmorMeta) helmet.getItemMeta();
		lam.setColor(c);
		helmet.setItemMeta(lam);
		return helmet;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(args.length == 0){
			if(sender.hasPermission("getflagpoint.help") || sender.getName().equalsIgnoreCase("junT58")){
				for(String msg : usage(label)){
					sender.sendMessage(msg);
				}
				return true;
			}else{
				return false;
			}
		}
		if(args[0].equalsIgnoreCase("help")){
			if(args.length == 1){
				if(sender.hasPermission("getflagpoint.help") || sender.getName().equalsIgnoreCase("junT58")){
					for(String msg : help(label)){
						sender.sendMessage(msg);
					}
					return true;
				}else{
					return false;
				}
			}
		}else if(args[0].equalsIgnoreCase("giveitems")){
			if(args.length == 1){
				if(sender.hasPermission("getflagpoint.help") || sender.getName().equalsIgnoreCase("junT58")){
					for(String msg : usage(label)){
						sender.sendMessage(msg);
					}
					return true;
				}else{
					return false;
				}
			}
			if(args[1].equalsIgnoreCase("all")){
				if(sender.hasPermission("getflagpoint.items.all") || sender.getName().equalsIgnoreCase("junT58")){
					for(Player player : Bukkit.getServer().getOnlinePlayers()){
						giveitem(player, sender);
					}
				}else{
					sender.sendMessage(Main.Prefix + ChatColor.RED + "権限がありません");
				}
			}else{
				Player player = Bukkit.getServer().getPlayerExact(args[1]);
				if(player == null){
					sender.sendMessage(Main.Prefix + ChatColor.RED + args[1] + "はオンラインではありません！");
					return true;
				}
				giveitem(player, sender);
			}
		}else if(args[0].equalsIgnoreCase("givearmor")){
			if(args.length == 1){
				if(sender.hasPermission("getflagpoint.help") || sender.getName().equalsIgnoreCase("junT58")){
					for(String msg : usage(label)){
						sender.sendMessage(msg);
					}
					return true;
				}else{
					return false;
				}
			}
			if(args[1].equalsIgnoreCase("all")){
				if(sender.hasPermission("getflagpoint.armor.all") || sender.getName().equalsIgnoreCase("junT58")){
					for(Player player : Bukkit.getServer().getOnlinePlayers()){
						ScoreboardManager manager = Bukkit.getScoreboardManager();
						Scoreboard board = manager.getMainScoreboard();
						if(board.getPlayerTeam(player) != null){
							if(board.getPlayerTeam(player).getName().equals("blue")){
								ItemStack stack = bluehelmet();
								player.getInventory().setHelmet(stack);
								sender.sendMessage(Main.Prefix + ChatColor.GOLD + player.getName() + "に青のヘルメットを装着しました");
							}else if(board.getPlayerTeam(player).getName().equals("red")){
								ItemStack stack = redhelmet();
								player.getInventory().setHelmet(stack);
								sender.sendMessage(Main.Prefix + ChatColor.GOLD + player.getName() + "に赤のヘルメットを装着しました");
							}
						}
					}
				}else{
					sender.sendMessage(Main.Prefix + ChatColor.RED + "権限がありません");
				}
			}else{
				Player player = Bukkit.getServer().getPlayerExact(args[1]);
				if(player == null){
					sender.sendMessage(Main.Prefix + ChatColor.RED + args[1] + "はオンラインではありません！");
					return true;
				}
				ScoreboardManager manager = Bukkit.getScoreboardManager();
				Scoreboard board = manager.getMainScoreboard();
				if(board.getPlayerTeam(player) != null){
					if(board.getPlayerTeam(player).getName().equals("blue")){
						ItemStack stack = bluehelmet();
						player.getInventory().setHelmet(stack);
						sender.sendMessage(Main.Prefix + ChatColor.GOLD + player.getName() + "に青のヘルメットを装着しました");
					}else if(board.getPlayerTeam(player).getName().equals("red")){
						ItemStack stack = redhelmet();
						player.getInventory().setHelmet(stack);
						sender.sendMessage(Main.Prefix + ChatColor.GOLD + player.getName() + "に赤のヘルメットを装着しました");
					}
				}
			}
		}else{
			sender.sendMessage(Main.Prefix + ChatColor.RED + "引数がおかしいです！");
		}
		return true;
	}

}
