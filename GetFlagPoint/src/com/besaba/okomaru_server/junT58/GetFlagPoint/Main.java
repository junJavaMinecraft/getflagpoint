package com.besaba.okomaru_server.junT58.GetFlagPoint;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{

	public static String Prefix = ChatColor.GRAY + "[" + ChatColor.GOLD + "GFP" + ChatColor.GRAY + "]";

	public void onEnable(){
		getCommand("gfp").setExecutor(new GfpCommand());
		getServer().getPluginManager().registerEvents(new DeathEvent(), this);
	}

}
